provider "aws" {
  region = "eu-west-2"  # Change this to your AWS region
}

# resource "tls_private_key" "this" {
#   algorithm = "RSA"
# }

# resource "aws_key_pair" "ssh-key" {
#   key_name   = "opendata-NN-key"
#   public_key = tls_private_key.this.public_key_openssh
# }

variable "private_key" {}
variable "public_key" {}
variable "password" {}
variable "user_data" {}

resource "aws_instance" "dash_app" {
  ami                    = "ami-0ec5504772420de7e"  # Replace with the correct AMI ID for your region
  instance_type          = "t2.micro"
  key_name               = "atlasopendata-ML-dash-v2"
  security_groups        = ["opendata-security"]
  associate_public_ip_address = true
  user_data              = file(var.user_data)


  tags = {
    Name = "DashApp"
  }

  provisioner "remote-exec" {       									
      inline = [
#   user_data = <<-EOF
#     #!/bin/bash
    "sudo sed -i \"/#\\$nrconf{restart} = 'i';/s/.*/\\$nrconf{restart} = 'a';/\" /etc/needrestart/needrestart.conf",
    "cat /etc/needrestart/needrestart.conf",
    "echo ==========================",
    "echo Updating, upgrading etc",
    "echo ==========================",
    "sudo apt update",
    "sudo apt upgrade -y",
    "echo ==========================",
    "echo Installing Docker",
    "echo ==========================",
    "sudo apt --yes install docker.io",
    "echo ==========================",
    "sudo docker run -d -p 8080:8080 atlasopendata/atlasopendata-dash-app"
      ]

    connection {					
      type          = "ssh"
      user          = "ubuntu"
      password      = var.password
      host          = self.public_ip
    }
  }
#   EOF
}

# resource "aws_security_group" "example" {
#   name        = "opendata-security"
#   description = "ssh"

#   ingress {
#     from_port   = 22
#     to_port     = 22
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   ingress {
#     from_port   = 443
#     to_port     = 443
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   ingress {
#     from_port   = 80
#     to_port     = 80
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }
#  }


output "instance_public_ip" {
  value = aws_instance.dash_app.public_ip
}
